# Cliente {#sec:cliente}

O cliente é o programa através do qual os utilizadores comunicam com e usufruem
das capacidades do sistema. A comunicação com o servidor é protocolada e
dividida em dois componentes: Servidor $\rightarrow$ Cliente e Cliente
$\rightarrow$ Servidor. Já que a comunicação é assíncrona, estes componentes
correm em paralelo.

Inputs do cliente que não correspondam a nenhum comando disponível no sistema ou
correspondem ao comando `quit` são tratados pelo próprio cliente, uma vez que
não é necessária qualquer intervenção do servidor. No caso de `quit`, o programa
termina a execução. No caso de um input desconhecido, é apresentada a lista de
comandos suportados pelo sistema.

Para os restantes comandos, o input é enviado, numa linha apenas, para o
servidor, recorrendo a um **PrintWriter**. Os comandos estão devidamente
protocolados logo não há necessidade de adaptar o input antes do envio.

As respostas do servidor são processadas por um **ReplyHandler** que tem acesso
ao canal de comunicação servidor $\rightarrow$ cliente e é inicializado e
colocado a correr numa _thread_ paralela aquando do início do programa. Este
continua em execução até ao término do programa.

O **ReplyHandler*** lê todas as respostas enviadas pelo servidor e executa as
operações que lhes correspondem. Apenas apresenta informação ao cliente no caso
de se tratar de mensagens do tipo _Reply_ ou _Error_.
