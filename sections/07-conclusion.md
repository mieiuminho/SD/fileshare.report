# Conclusão {#sec:end}

Os objetivos deste projeto foram alcançados, uma vez que, através da aplicação
prática dos conhecimentos adquiridos na Unidade Curricular, foram criadas
respostas de implementação para os desafios apresentados inicialmente pela
equipa docente.

Este projeto levou a um entendimento mais profundo dos componentes dos sistemas
distribuídos, tais como, _threads_, instruções assíncronas e manipulação de
regiões de memória partilhadas por processos concorrentes.

