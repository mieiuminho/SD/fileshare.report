# Model {#sec:model}

Toda a informação referente ao sistema é guardada num objeto da classe
**FileShare** que preconiza o _model_, agregando em si toda a informação e
operações relevantes.

Uma música é representada no sistema por um objeto da classe **Song**. Este
contém toda a informação referente à mesma: identificador único, título, autor,
ano, número de downloads, categorias e número de downloads.

Um utilizador é representado no sistema por um objeto da classe **User**. Este
contém toda a informação referente ao mesmo: nome de utilizador, password e a
_seed_ usado aquando da aplicação da função de hash (o campo _seed_ é usado para
que exista ainda mais variabilidade nas versões _hashed_ guardadas das
palavras passe, no sentido em que, desta forma duas password geram sequências
diferentes, uma vez que a função de _hash_ é aplicada após a concatenação da
password com a _seed_).

O **FileShare** guarda as informações sobre músicas e utilizadores em dois
**Maps**, onde as _keys_ são o identificador único da classe **Song** e o nome
de utilizador da classe **User**, respetivamente. Contém ainda constantes
importantes para o sistema e variáveis de instância que auxiliam o seu
funcionamento correto, nomeadamente um contador das músicas a serem transferidas
e um contador de todas as músicas adicionadas ao sistema que permite gerar o
identificador único.

Uma música é adicionada ao sistema logo que há um comando para o fazer e antes
mesmo da transferência do ficheiro, esta é inicializada com uma variável de
instância `aval` a `false`, o que a torna invisível até que todo o conteúdo do
ficheiro esteja presente no servidor e seja possível realizar o seu download. De
modo a não bloquear outras operações e à incerteza de quando ou se a
transferência de _bytes_ será finalizada com sucesso. Uma **Song** tem ainda um
limite temporal para terminar o _upload_. Se o sistema receber a confirmação da
transferência antes deste limite, `aval` é colocado a `true`, tornando-a assim
totalmente disponível para os utilizadores.

O **FileShare** garante a execução paralela de várias operações pelo que são
usados **Locks** (assim como na classe **Song**) e **Conditions** para gerir as
várias _threads_ e garantir o correto funcionamento do sistema, mesmo em
situações de concorrência.
