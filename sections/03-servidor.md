# Servidor {#sec:servidor}

O servidor é um componente nuclear da plataforma, sendo responsável por receber
e satisfazer os mais diversos pedidos dos vários utilizadores.

De modo a atender múltiplos clientes em simultâneo, cada conexão é administrada
por uma _thread_ única e identificável. Cada _thread_ corre a classe **Session**
(que implementa **Runnable**) que recebe aquando da sua criação o **FileShare**
do servidor, garantido assim que todos clientes operam sobre o mesmo objeto
**FileShare**.

A ligação é estabelecida ainda no servidor e o canal de comunicação entre o
servidor e o cliente (que toma a forma de um **PrintWritter**) é guardado num
**Map**, onde a _key_ é o número da _thread_ correspondente à conexão.

O input é recolhido pela classe **Session** associada a cada cliente e este, se
válido e necessário, é enviado para processamento através de um
**RequestBuffer**.  Comandos de autenticação são processados no **Session** e
não requerem o uso do **RequestBuffer**.

O **RequestBuffer** é único e partilhado, assim como o **FileShare**, e funciona
como uma linha de montagem, recebendo os inputs das várias _threads_ **Session**
(devidamente identificadas) e armazenando-os para que estes sejam processados
pelo sistema.

Este processamento é feito pela classe **Worker**, que tem acesso ao
**RequestBuffer** assim como ao **Map** de **PrintWritters**.

Para cada elemento lido do Buffer:

1. A _thread_ de origem é identificada.
2. O input é lido e interpretado.
3. Se se tratar de uma mensagem prevista no protocolo, são executadas as
     operações correspondentes.
4. Uma resposta, ou mais, são enviadas através do **PrintWritter**
     correspondente.
5. Por último, de modo a preservar o espaço ocupado pelos ficheiros,
     periodicamente é executada uma classe **Cleaner** que elimina todos os
     ficheiros obsoletos e não utilizados da memoria do servidor.
