# Funcionalidade Adicional {#sec:func_adicional}

## Limite de descargas

De modo a limitar o número de descargas simultâneas no sistema, recorremos ao
uso de _locks_ e _conditions_ no **FileShare**. Definimos `MAXDOWN` como uma
constante (`final static int`) e com valor arbitrário 5.

Temos no **FileShare** uma variável de instância `downloading` que funciona como
um contador e uma **Condition** `isCrowded`. Uma _thread_ que entre no método
`download()` verifica se `downloading` é igual a `MAXDOWN`. Se for o caso,
espera (`isCrowded.await()`) que uma descarga acabe. Caso contrário prossegue a
execução, incrementando a variavel `downloading`.

De notar que uma vez que os downloads são compartimentados em _chunks_ e o
método `download()` processa um _chunk_ de cada vez, um novo download é
identificado pelo parâmetro `offset`. Só testamos a condição e incrementamos o
`downloading` se `offset == 0` (novo download) e só decrementamos o contador no
último _chunk_ do ficheiro a ser descarregado (`offset == size/MAXSIZE`).

De modo a ser justo com todos os utilizadores, implementamos uma política
baseada numa _Priority Queue_. Todos os pedidos de download são primeiro
inseridos num **PriorityBuffer** que associa uma prioridade a cada utilizador e
reorganiza os pedidos a serem enviados ao **FileShare**. Quanto mais downloads
pendentes de um dado utilizador, menor será a prioridade dos seus pedidos.

Os pedidos estão constantemente a ser lidos e processados, vindos do _buffer_.
Quando chega um com prioridade maior, este é colocado “na frente da fila”, de
modo a garantir a justiça no atendimento a todos os utilizadores.

## Notificação

Todos os processos de _upload_ são finalizados pelo envio por parte do cliente
de uma mensagem do tipo **NOTIFY**. O servidor ao receber esta mensagem
disponibiliza a música no sistema. Aproveitando este facto, o servidor ao
processar estas mensagens recolhe a informação relevante e notifica todos os
utilizadores a usar a plataforma naquele momento que uma nova música está
disponível.

Uma vez que os processos de _download_ e _upload_ são fracionados em diversas
mensagens, é possível notificar os clientes com o normal envio da mensagem, não
sendo necessário nenhum ajuste. Nenhuma operação é bloqueante devida à natureza
do protocolo e do sistema em si.

## Tamanho dos ficheiros ilimitado

Definimos a constante `MAXSIZE` que representa o tamanho máximo que a
transferência de uma música pode ocupar tanto no cliente como no servidor.

Num _download_ ou _upload_ de um ficheiro é calculado o tamanho do mesmo. Se
este for menor que `MAXSIZE` o ficheiro cabe todo em memoria e pode ser enviado
recorrendo apenas a uma mensagem **DATA**. Esta mensagem terá como parâmetro
`offset` o valor 0.

Se o ficheiro não couber todo em memoria, este é dividido em _chunks_ de tamanho
`MAXSIZE`. Sequencialmente é lido um _chunk_, codificado e envidado sob a forma
de uma mensagem **DATA**. Os pontos de divisão serão enviados como parâmetro
`offset` de modo a ser possível recriar o ficheiro no destino. Por exemplo, os
bytes correspondentes ao segundo _chunk_ serão escritos no ficheiro com um
_offset_ de `MAXSIZE`. Depois do envio de um _chunk_ a memoria é libertada e
outro _chunk_ é lido. O processo é igual nos casos de _download_ e _upload_.

No destino, cada mensagem **DATA** é processada individualmente e
sequencialmente pelo que não há possibilidade de ficarem 2 _chunks_ em memoria.
Por exemplo, para o download um ficheiro com dois _chunks_, o cliente iria ler a
primeira mensagem **DATA**, escreveria no ficheiro com o _offset_ parametrizado
na mensagem, libertaria a memória e faria o mesmo para a segunda mensagem
**DATA**.

