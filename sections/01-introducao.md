# Introdução {#sec:intro}

No âmbito da unidade curricular de Sistemas Distribuídos, foi-nos proposta a
realização de um trabalho prático de modo a pôr em prática as competências e
conhecimentos adquiridos durante o semestre. O referido trabalho prático
consiste no desenvolvimento e implementação de uma plataforma de partilha de
ficheiros de música sob a forma de Cliente/Servidor em Java, utilizando
_sockets_ e _threads_.

Identificamos imediatamente o protocolo de comunicação cliente/servidor, a
concorrência das conexões e operações como sendo os principais aspetos a abordar
na implementação da plataforma.
