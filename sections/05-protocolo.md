# Protocolo {#sec:protocolo}

Todo o funcionamento do sistema depende da capacidade dos programas (servidor e
cliente) comunicarem entre si de forma constante e interpretável.  Para tal foi
desenvolvido um protocolo que rege as mensagens enviadas entre os dois. Há
pequenas diferenças nos tipos de mensagens enviadas do cliente para o servidor,
e vice-versa. Segue-se uma enumeração das mensagens presentes no protocolo bem
como uma pequena descrição de como estas são tratadas/processadas pelos
respetivos receptores. Importante notar que existem outros inputs válidos que
não estão incluídos por serem tratados imediatamente pelo programa _Cliente_ e
não necessitarem de ser enviados para o servidor.

**Comunicação Cliente** $\rightarrow$ **Servidor:**

  * `register <username> <password>` $\rightarrow$ Regista um novo utilizador no
sistema, com os argumentos fornecidos. Se for bem sucedido envia uma mensagem de
sucesso do tipo **REPLY**. Se não, envia uma mensagem **ERROR**.

  * `login <username> <login>` $\rightarrow$ Autentifica o utilizador. Se for
bem sucedido envia uma mensagem de sucesso do tipo **REPLY**. Se não, envia uma
mensagem **ERROR**.

  * `download <SongID>` $\rightarrow$ Envia o conteúdo do ficheiro para o
cliente (em várias mensagens se for necessário). Se for bem sucedido envia uma
mensagem de sucesso do tipo **REPLY**. Se não, envia uma mensagem **ERROR**.

  * `search <Tag>` $\rightarrow$ Pesquisa informações sobre todas as músicas com
a _tag_ fornecida. Se for bem sucedido envia uma mensagem do tipo **REPLY**
contendo essa informação. Se não, envia uma mensagem **ERROR**.

  * `upload <filePath> <title> <artist> <year> <tag> … ` $\rightarrow$ Cria o
objeto **Song** e adiciona-o ao sistema. Se for bem sucedido envia uma mensagem
do tipo **REQUEST** (abordado mais abaixo). Se não, envia uma mensagem **ERROR**.

  * `data <songID> <offset> <bytes in Base64>` $\rightarrow$ Escreve, depois de
decodificados, os bytes no ficheiro correspondente ao _songID_
(data/songID.mp3), com o _offset_ fornecido.

  * `notify <songID>` $\rightarrow$ Notifica todos os utilizadores que há uma
nova música disponível. Torna a **Song** correspondente ao _songID_ disponível
(`aval=true`).


**Comunicação Servidor** $\rightarrow$ **Cliente:**

  * `DATA <fileName> <offset> <bytes in Base64>` $\rightarrow$  Escreve, depois
de decodificados, os bytes no ficheiro correspondente ao _fileName_
(downloads/fileName.mp3), com o _offset_ fornecido.

  * `REQUEST <SongId> <filepath>` $\rightarrow$ Envia o conteúdo do ficheiro
_filepath_ sob a forma de uma ou mais mensagens do tipo **DATA** com o parâmetro
_SongID_ e _offset_ apropriado. Se for bem sucedido envia, por fim, uma mensagem
do tipo **NOTIFY** com o parâmetro _songID_.

  * `REPLY <message>` $\rightarrow$ Apresenta a _message_ ao utilizador, fazendo
_print_ da mesma no ecrã.

  * `ERROR <message>` $\rightarrow$ Apresenta a mensagem de erro _message_,
fazendo _print_ da mesma no ecrã.

